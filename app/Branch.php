<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    protected $fillable = ["branch_type_id", "user_id", "name",	"address", "phone", "fax", "contact", "celular", "email", "is_delete"];

    public function branches()
    {
    	return $this->belongsTo('App\BranchType', 'branch_type_id');
    }
}
