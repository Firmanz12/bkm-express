<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $table = 'transactions';
	protected $fillable = ["invoice", "user_id", "user_email", "branch_id", "receiver_name", "receiver_email", "receiver_address", "due_date", "status"];
}
