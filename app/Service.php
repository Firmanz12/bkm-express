<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $table = 't_services';
    protected $fillable = ['name', 'price'];

}
