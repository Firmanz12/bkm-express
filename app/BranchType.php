<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchType extends Model
{
    protected $table = 'branch_type';

    public function types()
    {
    	return $this->hasMany('App\Branch', 'branch_type_id');
    }
}
