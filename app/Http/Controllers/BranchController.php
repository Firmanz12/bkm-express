<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\BranchType;

class BranchController extends Controller
{
	protected $page = "branch";
	public function index()
	{
		$data['branches'] = Branch::with('branches')->join('branch_type', 'branch.branch_type_id', '=', 'branch_type.id')->where('is_delete', '=', false)->get(['branch_type.id as type_id','branch_type.name as type_name', 'branch.*']);
		return view('dashboard.branch.list_branch', $data);
	}
	public function addBranch()
	{
		$data['types'] = BranchType::get();
		$data['page'] = $this->page;
		return view('dashboard.branch.add_branch', $data);
	}

	public function storeBranch(Request $request)
	{
		// dd($request->all());
		$validate = $request->validate([
			"type" => 'required',
			"name" => 'required',
			"address" => 'required',
			"phone" => 'required|numeric',
			"fax" => 'required',
			"contact" => 'required',
			"celular" => 'required|numeric',
		]);
		$branch = new Branch;
		$branch->branch_type_id = $request->input('type');
		$branch->name = $request->input('name');
		$branch->address = $request->input('address');
		$branch->phone = $request->input('phone');
		$branch->fax = $request->input('fax');
		$branch->contact = $request->input('contact');
		$branch->celular = $request->input('celular');
		$branch->email = $request->input('email');
		$branch->is_delete = false;
		$branch->save();

		return redirect()->route('list-branch');
	}

	public function editBranch($id)
	{
		$branch = Branch::findOrFail($id)->first();

		$data['branches'] = $branch;
		$data['types'] = BranchType::get();
		return view('dashboard.branch.edit_branch', $data);
	}

	public function updateBranch($id, Request $request)
	{
		$validate = $request->validate([
			"type" => 'required',
			"name" => 'required',
			"address" => 'required',
			"phone" => 'required|numeric',
			"fax" => 'required',
			"contact" => 'required',
			"celular" => 'required|numeric',
		]);
		$branch = Branch::findOrFail($id)->first();

		$branch->branch_type_id = $request->input('type');
		$branch->name = $request->input('name');
		$branch->address = $request->input('address');
		$branch->phone = $request->input('phone');
		$branch->fax = $request->input('fax');
		$branch->contact = $request->input('contact');
		$branch->celular = $request->input('celular');
		$branch->email = $request->input('email');
		$branch->is_delete = false;
		$branch->save();

		return redirect()->route('list-branch');
	}

	public function deleteBranch($id)
	{
		$branch = Branch::findOrFail($id)->first();
		$branch->is_delete = true;
		$branch->save();

		return redirect()->route('list-branch');
	}
}
