<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Branch;
use App\Service;

class TransactionController extends Controller
{

	public function index()
	{
		return view('dashboard.transaction.list_transaction');
	}

	public function addTransaction()
	{
		$data['branches'] = Branch::all();
		$data['services'] = Service::all();
		$data['invoices'] = Transaction::select('invoice')->orderby('created_at', 'desc')->first();
		return view('dashboard.transaction.add_transaction', $data);
	}

	public function storeTransaction(Request $request)
	{
		$transaction = new Transaction;
		$validate = $request->validate([
			'branch' => 'required',
			'service_name' => 'required',
			'weight' => 'required',
			'quantity' => 'required',
			'subtotal' => 'required',
			'ddate' => 'required',
			'rec_name' => 'required',
			'rec_phone' => 'required',
			'rec_address' => 'required',
			'total' => 'required',
		]);
		foreach ($request->input('service_name') as $services) {
			$transaction->invoice = $request->input('invoice');
			$transaction->user_id = session('user_id');
			$transaction->user_email = session('email');
			$transaction->branch_id = $request->input('branch');
			$transaction->receiver_name = $request->input('rec_name');
			$transaction->receiver_email = $request->input('rec_phone');
			$transaction->receiver_address = $request->input('rec_address');
			$transaction->due_date = date("Y-m-d", strtotime($request->input('ddate')));
			$transaction->status = 1;
			$transaction->total = $request->input('total');
			$transaction->save();
		}

		return redirect()->route('add-transaction');
	}

}
