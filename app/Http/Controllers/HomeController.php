<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $api_url = "";
    protected $provider_url = "";
    protected $page = "home";
    public function __construct()
    {
        $this->api_url = env('WEB_PROVIDER_URL').'/api/';
    }

    public function home()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['page'] = $this->page;
        return view('home', $data);
    }

    public function logout()
    {
        session()->flush();
        $url = env('WEB_PROVIDER_URL');
        return redirect()->to($url.'logout?continue='.url("/"));
    }

    public function login()
    {
        $url = env('WEB_PROVIDER_URL');
        return redirect()->to($url.'login?continue='.url('/'));
    }

    public function register()
    {
        $url = env('WEB_PROVIDER_URL');
        return redirect()->to($url.'register?continue='.url('/'));
    }
}
