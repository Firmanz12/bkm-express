<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    //
    protected $api_url = "";
    function __construct()
    {
    	$this->api_url = env('WEB_PROVIDER_URL').'/api/';
    }

    public function index()
    {
    	// dd('ss');
    	$data['services'] = Service::all();
    	$data['page'] = "service";
    	// dd($data->data);
    	return view('dashboard.service.service', $data);
    }

    public function addService(Request $request)
    {
    	$service = new Service;
    	$service->name = $request->input('name');
    	$service->price = $request->input('price');
        $service->duration = $request->input('duration');
    	$service->save();

    	return redirect()->route('service');
    }

    public function update($id, Request $request)
    {
        $service = Service::findOrFail($id);
        $service->name = $request->input('name');
        $service->price = $request->input('price');
        $service->duration = $request->input('duration');
        $service->save();
        return redirect()->route('service');
    }
}
