<?php

namespace App\Http\Middleware;

use Closure;

class ChekIsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $api_url = env('API_URL').'/api/';
            $headers = array(
                "Accept: application/json"
            );
            if (!empty($request->get('bkmt'))) {
                $token = $request->get('bkmt');
                if (!empty($request->get('bkmt')) && !session()->get('token')) {
                    session(['token' => $token]);
                }
                if (!empty(session()->get('token'))) {
                    $check_session = doCurl($api_url.'auth/session?access_token='.$token, $headers);
                    if ($check_session->code == 200) {
                        $req_data = doCurl($api_url.'user/request-data?with_token=true&access_token=='.session()->get('token'), $headers);
                        if ($req_data->code == 200) {
                            $session = [
                                "user_name" => $req_data->data->name,
                                "email" => $req_data->data->email,
                                "user_id" => $req_data->data->id
                            ];
                            session($session);
                        }
                    }else{
                        session()->flush();
                        $url = env('API_URL');
                        return redirect()->to($url.'login?continue='.url('/'));
                    }
                }
                return $next($request);
            }else{
                if (!empty(session()->get('token'))) {
                    $token = session()->get('token');
                    $check_session = doCurl($api_url.'auth/session?access_token='.$token, $headers);
                    if ($check_session->code == 200) {
                        $req_data = doCurl($api_url.'user/request-data?with_token=true&access_token='.session()->get('token'), $headers);
                            if ($req_data->code == 200) {
                                $session = [
                                    "user_name" => $req_data->data->name,
                                    "email" => $req_data->data->email,
                                    "user_id" => $req_data->data->id
                                ];
                            session($session);
                        }
                    }else{
                        session()->flush();
                        $url = env('API_URL');
                        return redirect()->to($url.'login?continue='.url('/'));
                    }
                }else{
                    session()->flush();
                    $url = env('API_URL');
                    return redirect()->to($url.'login?continue='.url('/'));
                }
                return $next($request);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
