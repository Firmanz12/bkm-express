<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('address')->nullable();
            $table->string('phone',50)->nullable();
            $table->string('fax',10)->nullable();
            $table->string('contact')->nullable();
            $table->string('celular',50)->nullable();
            $table->string('email')->nullable();
            $table->boolean('is_delete', ['1', '0']);
            $table->timestamps();

            $table->foreign('agent_id')
                ->references('id')
                ->on('branch_type')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch');
    }
}
