<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice');
            $table->integer('user_id');
            $table->string('user_email');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->string('receiver_name');
            $table->string('receiver_email');
            $table->string('receiver_address');
            $table->date('due_date');
            $table->integer('status');
            $table->timestamps();
        });

        Schema::table('transactions', function(Blueprint $table){
            $table->foreign('branch_id')
                ->references('id')
                ->on('branch')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
