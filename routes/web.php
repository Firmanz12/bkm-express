<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/login', 'HomeController@login')->name('login');

	Route::group(['prefix' => 'branch'], function(){
		Route::get('/', "BranchController@index")->name('list-branch');
		Route::get('/edit/{id}', 'BranchController@editBranch')->name('edit-branch');
		Route::post('/edit/{id}', 'BranchController@updateBranch')->name('update-branch');
		Route::get('/delete/{id}', 'BranchController@deleteBranch')->name('delete-branch');

		Route::get('/add', "BranchController@addBranch")->name('add-branch');
		Route::post('/add', "BranchController@storeBranch")->name('store-branch');
	});

	Route::group(['prefix' => 'service'], function(){
		Route::get('/', 'ServiceController@index')->name('service');
		Route::post('/add', 'ServiceController@addService')->name('add-services');
		Route::post('/update/{id}', 'ServiceController@update')->name('update-services');
	});

	Route::group(["prefix" => "transaction"], function(){
		Route::get('/', "TransactionController@index")->name('list-transaction');
		Route::get('/add', "TransactionController@addTransaction")->name('add-transaction');
		Route::post('/add', "TransactionController@storeTransaction")->name('store-transaction');
		Route::get('/change-status', "TransactionController@changeStatusTransaction")->name('cancel-transaction');
	});
});
Route::get('/logout', 'HomeController@logout')->name('logout');