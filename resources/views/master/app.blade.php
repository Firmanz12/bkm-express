<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BKM EXPRESS</title>

    <!-- Styles -->
    <link href="{{ asset('dashboard/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="{{ asset('dashboard/css/lib/calendar2/semantic.ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/calendar2/pignose.calendar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/select2/css/select2.css') }}" rel="stylesheet">

</head>
</head>
<body class="fix-header fix-sidebar">
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b><img src="images/logo.png" alt="homepage" class="dark-logo" /></b>
                    </a>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-building"></i><span class="hide-menu">Branch Management</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('list-branch') }}">Branch List</a></li>
                                <li><a href="{{ route('add-branch') }}">Add Branch</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-building"></i><span class="hide-menu">Service Management</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('service') }}">Service List</a></li>
                                <li><a href="{{ route('add-transaction') }}">Add Transaction</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
            @yield('content')
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="{{ asset('dashboard/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('dashboard/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('dashboard/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('dashboard/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="{{ asset('dashboard/js/lib/morris-chart/raphael-min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/morris-chart/morris.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/morris-chart/dashboard1-init.js') }}"></script>


    <script src="{{ asset('dashboard/js/lib/calendar-2/moment.latest.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/semantic.ui.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/prism.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/pignose.calendar.min.js') }}"></script>
    <!-- scripit init-->
    <script src="{{ asset('dashboard/js/lib/calendar-2/pignose.init.js') }}"></script>

    <script src="{{ asset('dashboard/js/lib/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('dashboard/js/lib/owl-carousel/owl.carousel-init.js') }}"></script>
    <script src="{{ asset('dashboard/js/scripts.js') }}"></script>
    <!-- scripit init-->

    <script src="{{ asset('/dashboard/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/dashboard/js/lib/datatables/datatables-init.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#branch').change(function(){

            });

            $('#btn-add-row').click(function(){
                var row = 0;
                $('#service-table tr:last').after(`
                    <tr>
                        <td>
                            <select name="service_name[]" class="form-control service_name">
                                <option value="">  </option>
                                @if(!empty($services))
                                    <?php foreach ($services as $service): ?>
                                        <option value="{{ $service->id }}"> {{ $service->name.' - '.$service->price.' - '.$service->duration.' day' }} </option>
                                    <?php endforeach ?>
                                @endif
                            </select>
                            <input type="hidden" class="selected_duration">
                        </td>
                        <td>
                            <input type="number" name="weight[]" class="form-control weight" value="1">
                        </td>
                        <td><input type="number" name="quantity[]" class="form-control quantity" value="1"></td>
                        <td>
                            <input type="number" name="subtotal[]" class="form-control subtotal" readonly="true">
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>      
                        </td>
                    </tr>
                `)
                row++
            });
            $(document).ready(function(){
                $('#ddate').datepicker({
                    format: 'mm/dd/yyyy'
                })
            })
            $(document).on('click', 'button.delete-row', function(){
                $(this).closest('tr').remove();
            });
            $(document).on('change', 'select.service_name', function(){
                var duration = 0;

                var element = <?php if(!empty($services)){echo $services;}else{echo "";} ?>;
                var id = $(this).val();
                element = $.grep(element, function(e) {
                    return e.id == id;
                });
                var weight = $(this).closest('tr').find('input.weight').val();
                var quantity = $(this).closest('tr').find('input.quantity').val();
                var total = (element[0]["price"] * weight) * quantity;
                $(this).closest('tr').find('input.subtotal').val(total);
                $(this).closest('tr').find('input.selected_duration').val(element[0]["duration"]);

                var subtotal = 0;
                $('#service-table').find('tr').find('input.subtotal').each(function(i, el){
                    subtotal = (parseInt(subtotal) + parseInt($(this).val()));
                    $('#total').val(subtotal)
                });
            });
            $(document).on('change', 'input.weight', function(){
                var selected_service = $(this).closest('tr').find('select.service_name').val();
                var element = <?php if(!empty($services)){echo $services;}else{echo "";} ?>;
                element = $.grep(element, function(e) {
                    return e.id == selected_service;
                });

                var quantity = $(this).closest('tr').find('input.quantity').val();
                var total = (element[0]["price"] * $(this).val()) * quantity;
                $(this).closest('tr').find('input.subtotal').val(total);
                var subtotal = 0;
                $('#service-table').find('tr').find('input.subtotal').each(function(i, el){
                    subtotal = (parseInt(subtotal) + parseInt($(this).val()));
                    $('#total').val(subtotal)
                });
            });
            $(document).on('change', 'input.quantity', function(){
                var selected_service = $(this).closest('tr').find('select.service_name').val();
                var element = <?php if(!empty($services)){echo $services;}else{echo "";} ?>;
                element = $.grep(element, function(e) {
                    return e.id == selected_service;
                });

                var weight = $(this).closest('tr').find('input.weight').val();
                var total = (element[0]["price"] * weight) * $(this).val();
                $(this).closest('tr').find('input.subtotal').val(total);
                var subtotal = 0;
                $('#service-table').find('tr').find('input.subtotal').each(function(i, el){
                    subtotal = (parseInt(subtotal) + parseInt($(this).val()));
                    $('#total').val(subtotal)
                });
            });
        });
    </script>
</body>
</html>