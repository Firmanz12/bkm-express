@extends('master.app')

@section('content')
<div class="main-wrapper">
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">Service List</h3>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-content">
                                <h4 class="card-title">Service List</h4>
                                <button id="add-services" class="btn btn-primary" data-target="#add_service_modal" data-toggle="modal">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                                <div class="table-responsive m-t-40" style="margin-top: 20px!important">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@lang('layout.data.service-name')</th>
                                                <th>@lang('layout.data.service-price')</th>
                                                <th>Durasi (dalam hari)</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($services))
                                                <?php $i =1; foreach ($services as $service): ?>
                                                    <tr class="centered">
                                                        <td>{{ $i }}</td>
                                                        <td>{{ $service->name }}</td>
                                                        <td>{{ $service->price }}</td>
                                                        <td>{{ $service->duration }}</td>
                                                        <td>
                                                            <button class="btn btn-primary" data-target="#edit_service_modal_{{ $service->id }}" data-toggle="modal"> <i class="fa fa-pencil"></i> </button>
                                                        </td>
                                                    </tr>
                                                <?php $i++; endforeach ?>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($services as $service): ?>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTitle" id="edit_service_modal_{{ $service->id }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle">Tambah Layanan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form method="POST" action="{{ route('update-services', ['id' => $service->id]) }}" enctype="multipart/form-data">
      {{csrf_field()}}
        <div class="modal-body">
            <div class="box-body">
                @if ($errors->has('name'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="judul_event">Nama Layanan</label>
                    @if ($errors->has('name'))
                        <input type="text" name="name" class="form-control" id="role-name" placeholder="Nama Layanan" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('name') }}</div>
                    @else
                        <input type="text" name="name" class="form-control" id="role-name" placeholder="Nama Layanan" value="{{ old('name', $services ? $service->name : null) }}">
                    @endif

                </div>
                
                @if ($errors->has('price'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="price">Harga per/kg</label>
                    @if ($errors->has('price'))
                        <input type="number" name="price" class="form-control" id="role-value" placeholder="Harga per/kg" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('price') }}</div>
                    @else
                        <input type="number" name="price" class="form-control" id="role-value" placeholder="Harga per/kg" value="{{ old('price', $services ? $service->price : null) }}">
                    @endif
                </div>

            @if ($errors->has('duration'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="duration">Durasi (hari)</label>
                    @if ($errors->has('duration'))
                        <input type="number" name="duration" class="form-control" id="role-value" placeholder="Durasi (dalam hari)" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('duration') }}</div>
                    @else
                        <input type="number" name="duration" class="form-control" id="role-value" placeholder="Durasi" value="{{ old('duration', $services ? $service->duration : null) }}">
                    @endif
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach ?>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTitle" id="add_service_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle">Tambah Layanan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form method="POST" action="{{ route('add-services') }}" enctype="multipart/form-data">
      {{csrf_field()}}
        <div class="modal-body">
            <div class="box-body">
                @if ($errors->has('name'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="judul_event">Nama Layanan</label>
                    @if ($errors->has('name'))
                        <input type="text" name="name" class="form-control" id="role-name" placeholder="Nama Layanan" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('name') }}</div>
                    @else
                        <input type="text" name="name" class="form-control" id="role-name" placeholder="Nama Layanan" value="{{ old('name') }}">
                    @endif

                </div>
                
                @if ($errors->has('price'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="price">Harga per/kg</label>
                    @if ($errors->has('price'))
                        <input type="number" name="price" class="form-control" id="role-value" placeholder="Harga per/kg" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('price') }}</div>
                    @else
                        <input type="number" name="price" class="form-control" id="role-value" placeholder="Harga per/kg" value="{{ old('price') }}">
                    @endif
                </div>

            @if ($errors->has('duration'))
                    <div class="form-group is-invalid">
                @else
                    <div class="form-group">
                @endif
                    <label for="duration">Durasi (hari)</label>
                    @if ($errors->has('duration'))
                        <input type="number" name="duration" class="form-control" id="role-value" placeholder="Durasi (dalam hari)" aria-invalid="true">
                        <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('duration') }}</div>
                    @else
                        <input type="number" name="duration" class="form-control" id="role-value" placeholder="Durasi" value="{{ old('duration') }}">
                    @endif
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection