@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
    	    <div class="col-md-5 align-self-center">
    	        <h3 class="text-primary">Tambah Branch</h3>
    	    </div>
    	</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
								<div class="form-validation">
									<form class="form-valide" method="POST" action="{{ route('store-transaction') }}">
										{{ csrf_field() }}
										<div class="col-12 row">
											<div class="col-md-6">
												<p>Invoice - Tanggal</p>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="invoice" readonly="" value="<?php if(!empty($invoices)){ echo $invoices->invoice;}else{ echo date('y/m/001');} ?>">
                                                </div>

												<div class="form-group">
													@if ($errors->has('branch'))
                                        		    	<div class="form-group row is-invalid">
                                        			@else
                                        			    <div class="form-group row">
                                        			@endif
                                        			    <label class="col-lg-2 col-form-label" for="val-type">Branch</label>
                                        			    <div class="col-lg-8">
                                        			        @if ($errors->has('branch'))
                                        			        	<select class="form-control" id="branch" name="branch" aria-invalid=true>
                                        			        	    <option value="">--Pilih Branch--</option>
                                        			        	    <?php foreach ($branches as $branch): ?>
                                        			        	        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        			        	    <?php endforeach ?>
                                        			        	</select>
                                        			        	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('branch') }}</div>
                                        			        @else
                                        			        	<select class="form-control" id="branch" name="branch" aria-invalid=true>
                                        			        	    <option value="">--Pilih Branch--</option>
                                        			        	    <?php foreach ($branches as $branch): ?>
                                        			        	        <?php if (old('branch') == $branch->id): ?>
                                        			        	            <option value="{{ $branch->id }}" selected="true">{{ $branch->name }}</option>
                                        			        	        <?php else: ?>
                                        			        	            <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                        			        	        <?php endif ?>
                                        			        	    <?php endforeach ?>
                                        			        	</select>
                                        			        @endif
                                        			    </div>
                                        			</div>
												</div>
												<p>Alamat</p>
												<b> - </b>
											</div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-bordered table-striped" id="service-table">
                                                        <thead>
                                                            <tr>
                                                                <th>Layanan</th>
                                                                <th>Berat (Kg)</th>
                                                                <th>Jumlah</th>
                                                                <th>SubTotal</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <select name="service_name[]" class="form-control service_name">
                                                                        <option value="">  </option>
                                                                        @if(!empty($services))
                                                                            <?php foreach ($services as $service): ?>
                                                                                <option value="{{ $service->id }}"> {{ $service->name.' - '.$service->price.' - '.$service->duration.' day' }} </option>
                                                                            <?php endforeach ?>
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input type="number" name="weight[]" class="form-control weight" value="1">
                                                                </td>
                                                                <td><input type="number" name="quantity[]" class="form-control quantity" value="1"></td>
                                                                <td>
                                                                    <input type="number" name="subtotal[]" class="form-control subtotal" readonly="true">
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>      
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" id="btn-add-row">
                                                        <i class="fa fa-plus"></i> Tambah Baris
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-12 row">
                                                <div class="col-md-6">
                                                    @if ($errors->has('ddate'))
                                                        <div class="form-group row is-invalid">
                                                    @else
                                                        <div class="form-group row">
                                                    @endif
                                                        <label for="ddate">Jatuh Tempo</label>
                                                        @if ($errors->has('ddate'))
                                                            <input type="date" id="ddate" name="ddate" class="form-control" data-provide="datepicker">
                                                            <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('ddate') }}</div>
                                                        @else
                                                            <input type="date" id="ddate" name="ddate" class="form-control" data-provide="datepicker">
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Penerima</label>
                                                        @if ($errors->has('rec_name'))
                                                            <div class="form-group row is-invalid">
                                                        @else
                                                            <div class="form-group row">
                                                        @endif
                                                            <input type="text" id="rec_name" name="rec_name" class="form-control" placeholder="Nama">
                                                            @if ($errors->has('rec_name'))
                                                                <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('rec_name') }}</div>
                                                            @endif
                                                        </div>

                                                        @if ($errors->has('rec_phone'))
                                                            <div class="form-group row is-invalid">
                                                        @else
                                                            <div class="form-group row">
                                                        @endif
                                                            <input type="text" id="rec_phone" name="rec_phone" class="form-control" placeholder="Alamat">
                                                            @if ($errors->has('rec_phone'))
                                                                <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('rec_phone') }}</div>
                                                            @endif
                                                        </div>

                                                        @if ($errors->has('rec_address'))
                                                            <div class="form-group row is-invalid">
                                                        @else
                                                            <div class="form-group row">
                                                        @endif
                                                            <textarea class="form-control" name="rec_address"></textarea>
                                                            @if ($errors->has('rec_address'))
                                                                <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('rec_address') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                     <div class="form-group">
                                                        <label for="total">Total</label>
                                                        <input type="text" id="total" name="total" class="form-control" readonly="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">
                                                            Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection