@extends('master.app')

@section('content')
<div class="main-wrapper">
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-primary">Service List</h3>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-content">
                                <h4 class="card-title">Service List</h4>
                                <button id="add-services" class="btn btn-primary" data-target="#add_service_modal" data-toggle="modal">
                                    <i class="fa fa-plus"></i> Tambah
                                </button>
                                <div class="table-responsive m-t-40" style="margin-top: 20px!important">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>@lang('layout.data.service-name')</th>
                                                <th>@lang('layout.data.service-price')</th>
                                                <th>Durasi (dalam hari)</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($services))
                                                <?php $i =1; foreach ($services as $service): ?>
                                                    <tr class="centered">
                                                        <td>{{ $i }}</td>
                                                        <td>{{ $service->name }}</td>
                                                        <td>{{ $service->price }}</td>
                                                        <td>{{ $service->duration }}</td>
                                                        <td>
                                                            <button class="btn btn-primary" data-target="#edit_service_modal_{{ $service->id }}" data-toggle="modal"> <i class="fa fa-pencil"></i> </button>
                                                        </td>
                                                    </tr>
                                                <?php $i++; endforeach ?>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection