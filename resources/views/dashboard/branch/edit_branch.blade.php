@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
    	    <div class="col-md-5 align-self-center">
    	        <h3 class="text-primary">Tambah Branch</h3>
    	    </div>
    	</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
								<div class="form-validation">
                                    <form class="form-valide" action="{{ route('update-branch', [$branches->id]) }}" method="post">
                                    	{{ csrf_field() }}
                                    	@if ($errors->has('name'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="name">@lang('layout.input.placeholder.branch.branch-name') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('name'))
                                                	<input type="text" class="form-control" id="name" name="name" placeholder="@lang('layout.input.placeholder.branch.branch-name')" aria-invalid="true">
                                                	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('name') }}</div>
                                                @else
                                                	<input type="text" class="form-control" id="name" name="name" placeholder="@lang('layout.input.placeholder.branch.branch-name')" aria-invalid="false" value="{{ $branches->name }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('type'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="val-type">Type <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('type'))
                                                <select class="form-control" id="type" name="type" aria-invalid=true>
                                                    <option value="">--Pilih Type--</option>
                                                    <?php foreach ($types as $type): ?>
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    <?php endforeach ?>
                                                </select>
                                                <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('type') }}</div>
                                                @else
                                                <select class="form-control" id="type" name="type" aria-invalid=true>
                                                    <option value="">--Pilih type--</option>
                                                    <?php foreach ($types as $type): ?>
                                                        <?php if ($branches->branch_type_id == $type->id): ?>
                                                            <option value="{{ $type->id }}" selected="true">{{ $type->name }}</option>
                                                        <?php else: ?>
                                                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                </select>
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('address'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="address">@lang('layout.input.placeholder.branch.address') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('address'))
                                                    <input type="text" class="form-control" id="address" name="address" placeholder="@lang('layout.input.placeholder.branch.address')" aria-invalid="true">
                                                    <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('address') }}</div>
                                                @else
                                                    <input type="text" class="form-control" id="address" name="address" placeholder="@lang('layout.input.placeholder.branch.address')" aria-invalid="false" value="{{ $branches->address }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('phone'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="phone">@lang('layout.input.placeholder.branch.phone') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('phone'))
                                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="@lang('layout.input.placeholder.branch.phone')" aria-invalid="true">
                                                    <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('phone') }}</div>
                                                @else
                                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="@lang('layout.input.placeholder.branch.phone')" aria-invalid="false" value="{{ $branches->phone }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('fax'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="fax">@lang('layout.input.placeholder.branch.fax') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('fax'))
                                                    <input type="text" class="form-control" id="fax" name="fax" placeholder="@lang('layout.input.placeholder.branch.fax')" aria-invalid="true">
                                                    <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('fax') }}</div>
                                                @else
                                                    <input type="text" class="form-control" id="fax" name="fax" placeholder="@lang('layout.input.placeholder.branch.fax')" aria-invalid="false" value="{{ $branches->fax }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('contact'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="contact">@lang('layout.input.placeholder.branch.contact') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('contact'))
                                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="@lang('layout.input.placeholder.branch.contact')" aria-invalid="true">
                                                    <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('contact') }}</div>
                                                @else
                                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="@lang('layout.input.placeholder.branch.contact')" aria-invalid="false" value="{{ $branches->contact }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('celular'))
                                            <div class="form-group row is-invalid">
                                        @else
                                            <div class="form-group row">
                                        @endif
                                            <label class="col-lg-2 col-form-label" for="celular">@lang('layout.input.placeholder.branch.celular') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('celular'))
                                                    <input type="text" class="form-control" id="celular" name="celular" placeholder="@lang('layout.input.placeholder.branch.celular')" aria-invalid="true">
                                                    <div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('celular') }}</div>
                                                @else
                                                    <input type="text" class="form-control" id="celular" name="celular" placeholder="@lang('layout.input.placeholder.branch.celular')" aria-invalid="false" value="{{ $branches->celular }}">
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('email'))
                                    		<div class="form-group row is-invalid">
                                    	@else
                                    		<div class="form-group row">
                                    	@endif
                                            <label class="col-lg-2 col-form-label" for="email">@lang('layout.input.placeholder.branch.email') <span class="text-danger">*</span></label>
                                            <div class="col-lg-8">
                                                @if ($errors->has('email'))
                                                	<input type="text" class="form-control" id="email" name="email" placeholder="@lang('layout.input.placeholder.branch.email')" aria-invalid="true">
                                                	<div id="val-email-error" class="invalid-feedback animated fadeInDown">{{ $errors->first('email') }}</div>
                                                @else
                                                	<input type="text" class="form-control" id="email" name="email" placeholder="@lang('layout.input.placeholder.branch.email')" aria-invalid="false" value="{{ $branches->email }}">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection