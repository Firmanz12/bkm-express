@extends('master.app')

@section('content')
<div class="main-wrapper">
	<div class="page-wrapper">
		<div class="row page-titles">
        	<div class="col-md-5 align-self-center">
        	    <h3 class="text-primary">Branch List</h3>
        	</div>
    	</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<div class="card-content">
                                <h4 class="card-title">Branch List</h4>
                                <div class="table-responsive m-t-40" style="margin-top: 20px!important">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>Telepon</th>
                                                <th>Fax</th>
                                                <th>Kontak</th>
                                                <th>No. Hp</th>
                                                <th>Email</th>
                                                <th>Tipe</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach ($branches as $branch): ?>
                                            	<tr>
                                                	<td>{{ $i }}</td>
                                                	<td>{{ $branch->name }}</td>
                                                	<td>{{ $branch->address }}</td>
                                                	<td>{{ $branch->phone }}</td>
                                                    <td>{{ $branch->fax }}</td>
                                                    <td>{{ $branch->contact }}</td>
                                                    <td>{{ $branch->celular }}</td>
                                                    <td>{{ $branch->email }}</td>
                                                    <td>{{ $branch->type_name }}</td>
                                                	<td>
                                                		<a href="{{ route('edit-branch', ['id' => $branch->id ]) }}">
                                                            <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>      
                                                        </a>
                                                		<a href="{{ route('delete-branch', ['id' => $branch->id ]) }}">
                                                            <button class="btn btn-danger"><i class="fa fa-trash"></i></button>      
                                                        </a>
                                                	</td>
                                            	</tr>
                                            <?php $i++; endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection