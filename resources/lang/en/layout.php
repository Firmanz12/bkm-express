<?php

return [

    /*
    |--------------------------------------------------------------------------
    | layout
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'menu' => [
        'user-list' => 'Pengguna',
        'service' => 'Layanan',
        'sales-management' => 'Sales Manajemen',
        'transaction-data' => 'Data Transaksi',
        'logout' => 'Logout'
    ],

    'input' => [
        'branch' => [
            'branch-name' => 'Nama Branch',
            'address' => 'Alamat',
            'phone' => 'Telepon',
            'fax' => 'Fax',
            'contact' => "Kontak",
            'celular' => "No. Hp",
            'email' => 'Email',
        ],
        'placeholder' => [
            'branch' => [
                'branch-name' => 'Nama Branch',
                'address' => 'Alamat',
                'phone' => 'Telepon',
                'fax' => 'Fax',
                'contact' => "Kontak",
                'celular' => "No. Hp",
                'email' => 'Email'
            ],
        ],

    ],

    'data' => [
        // service
        'service-name' => 'Layanan',
        'service-price' => 'Harga',
        'add-service' => 'Tambah Layanan',
    ]
];
